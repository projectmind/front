import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import { aliases, mdi } from "vuetify/iconsets/mdi";

// Vuetify
import 'vuetify/styles'
import { createVuetify, type ThemeDefinition } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import "@mdi/font/css/materialdesignicons.css";

const myCustomLightTheme: ThemeDefinition = {
  colors: {
    background: '#E7D8C9',
  }
}
const vuetify = createVuetify({
  components,
  directives,
  icons: {
    defaultSet: "mdi",
    aliases,
    sets: {
      mdi,
    },
  },
  // theme: {
  //   themes: {
  //     light: {
  //       colors: {
  //         background: "#E7D8C9",
  //       },
  //     },
  //   },
  // },
  theme: {
    themes: {
      myCustomLightTheme,
    }
  }
})
const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(vuetify);
app.mount('#app')
