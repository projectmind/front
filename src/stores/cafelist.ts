import type CafeList from "@/types/CafeList";
import { defineStore } from "pinia";
import cafeService from "@/services/cafelist"
import { ref, watch } from "vue";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
//import cafeService from "@/services/cafelist";
import router from "@/router";
import type Cafe from "@/types/Cafe";
import type Category from "@/types/Category";
export const useCafelistStore = defineStore("User", () => {
  const cafalist = ref<CafeList[]>([]);
  const cafalistDrink = ref<CafeList[]>([]);
  const cafalistCheckIn = ref<CafeList[]>([]);
  const cafalistBakery = ref<CafeList[]>([]);
  const cafalistEat = ref<CafeList[]>([]);
  const cafaAll = ref<Cafe[]>([]);
  const cafalistItem = ref<Cafe[]>([]);
  const cafalistDataItem = ref<Cafe[]>([]);
  const editedCategory = ref<Category>({
    name: ""
  });
  const name = ref("")
  const dialog = ref(false);
  const editedCafe = ref<Cafe & { files: File[] }>({
    cafename: "",
    description: "",
    location: "",
    srore: 0,
    categoryId: 1,
    image: "no_img_available.jpg",
    files: [],
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCafe.value = {
        cafename: "",
        description: "",
        location: "",
        srore: 0,
        categoryId: 1,
        image: "no_img_available.jpg",
        files: [],
      };
    }
  });


  // const cafalistItem = ref<CafeList[]>([]);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const iditem = ref(0)

  async function getCafeCategory_Drink(categoryId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await cafeService.getCafalistCategogy(categoryId);
      // users.value = res.data;
      cafalistDrink.value = res.data
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล cafelist ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getCafeCategory_ChackIN(categoryId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await cafeService.getCafalistCategogy(categoryId);
      // users.value = res.data;
      cafalistCheckIn.value = res.data
      console.log(cafalistCheckIn.value)
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล cafelist ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getCafeCategory_Bakery(categoryId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await cafeService.getCafalistCategogy(categoryId);
      // users.value = res.data;
      cafalistBakery.value = res.data
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล cafelist ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getCafeCategory_Eat(categoryId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await cafeService.getCafalistCategogy(categoryId);
      // users.value = res.data;
      cafalistEat.value = res.data
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล cafelist ได้");
    }
    loadingStore.isLoading = false;
  }


  async function getiditem(cafeId: Cafe) {
    loadingStore.isLoading = true;
    //iditem.value = cafeId
    //const des = ref()
    try {
      // const res = await cafeService.getCafeItem(cafeId);
      // users.value = res.data;
      // cafalistItem.value = res.data
      // cafalistItem.value = JSON.parse(JSON.stringify(cafalistItem.value))
      for (let i = 0; i < cafalistDataItem.value.length; i++) {
        cafalistDataItem.value[i].cafename = cafalistItem.value[i].cafename
        cafalistDataItem.value.push()
        cafalistDataItem.value[i].description = cafalistItem.value[i].description
        cafalistDataItem.value[i].image = cafalistItem.value[i].image
        cafalistDataItem.value[i].location = cafalistItem.value[i].location
        cafalistDataItem.value[i].srore = cafalistItem.value[i].srore
      }
      // const index = cafalistItem.value.findIndex((item) => item.id === cafeId);
      // cafalistDataItem.value.push();
      console.log(cafalistItem.value)
      //console.log(cafalistDataItem.value)
      router.push("/cafeItem"); //path
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล cafe ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getCafe() {
    loadingStore.isLoading = true;
    try {
      const res = await cafeService.getCafe();
      // users.value = res.data;
      cafaAll.value = res.data
      //cafaAll.value = JSON.parse(res.data)
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล cafe ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCafe() {
    loadingStore.isLoading = true;
    try {
      if (editedCafe.value.id) {
        const res = await cafeService.updateCafe(
          editedCafe.value.id,
          editedCafe.value
        );
      } else {
        const res = await cafeService.saveCafe(editedCafe.value);
      }

      dialog.value = false;
      await getCafe();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Cafe ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editCafe(cafe: Cafe) {
    editedCafe.value = JSON.parse(JSON.stringify(cafe));
    dialog.value = true;
  }
  async function deleteCafe(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await cafeService.deleteCafe(id);
      await getCafe();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Cafe ได้");
    }
    loadingStore.isLoading = false;
  }
  return {
    deleteCafe,
    editCafe,
    loadingStore,
    cafalist,
    cafalistDrink,
    messageStore,
    getCafeCategory_Drink,
    cafalistCheckIn,
    getCafeCategory_ChackIN,
    getCafeCategory_Bakery,
    cafalistBakery,
    cafalistEat,
    getCafeCategory_Eat,
    cafalistItem,
    getCafe,
    iditem,
    getiditem,
    cafalistDataItem,
    name,
    cafaAll,
    dialog,
    saveCafe,
    editedCafe,
    editedCategory
  };
});