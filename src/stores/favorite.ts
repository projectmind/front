import type Favorite from "@/types/Favorite";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import favoriteService from "@/services/favorite"
import type Cafe from "@/types/Cafe";
export const useFavoriteStore = defineStore("Favorite", () => {
  const Favorite = ref<Favorite[]>([]);

  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  async function getFavoriteByUser(userId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await favoriteService.getFavoriteByUser(userId);
      // users.value = res.data;
      Favorite.value = res.data
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Favorite ได้");
    }
    loadingStore.isLoading = false;
  }

  const dialog = ref(false)
  const clickFavoriteCafe = ref<Cafe>({
  });

  function clickFavorite(cafe: Cafe) {
    return clickFavoriteCafe.value = cafe
  }

  const userString = localStorage.getItem("user");
  const user = userString ? JSON.parse(userString) : null;
  const userId = user.id


  async function SaveReview() {
    loadingStore.isLoading = true;
    dialog.value = true;
    try {

      const editFavoite = ref<Favorite>({
        UsersId: userId,
        cafe: { id: clickFavoriteCafe.value.id },
        user: { id: userId },
      });
      const res = await favoriteService.saveFavorite(editFavoite.value)
      console.log(userId)
      console.log(user.name)
      messageStore.showError("บันทึกข้อมูล Favorite ได้");
    } catch (e) {
      console.log(e);
      console.log(clickFavoriteCafe.value.id)
      console.log(user.id)
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Favorite ได้");
    }

    loadingStore.isLoading = false;
  }
  return {
    dialog,
    Favorite,
    loadingStore,
    messageStore,
    getFavoriteByUser,
    clickFavoriteCafe,
    clickFavorite,
    SaveReview
  };
});