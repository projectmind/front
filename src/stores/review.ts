import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Review from "@/types/Review";
import { ref } from "vue";
import reviewService from "@/services/review"
import type ReviewList from "@/types/ReviewList";
import type Cafe from "@/types/Cafe";
export const useReviewStore = defineStore("Review", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const reviewbyuser = ref<Review[]>([]);
  const reviewlistbycafe = ref<ReviewList[]>([]);
  const dialog = ref(false)
  async function getReviewByUser(userId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await reviewService.getReviewByUser(userId);
      // users.value = res.data;
      reviewbyuser.value = res.data
      console.log(reviewbyuser.value)
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Review ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getReviewListByCafe(cafeId: number) {
    loadingStore.isLoading = true;
    dialog.value = true;
    try {
      const res = await reviewService.getReviewListByCafe(cafeId);
      // users.value = res.data;
      reviewlistbycafe.value = res.data
      console.log(reviewlistbycafe.value)
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล ReviewList ได้");
    }
    loadingStore.isLoading = false;
  }

  const userString = localStorage.getItem("user");
  const user = userString ? JSON.parse(userString) : null;
  const editReview = ref<Review>({
    message: "",
    srore: 0,
    UsersId: user.id,
    user: { id: user.id }
  });

  const reviewclick = ref<Cafe>({
  });
  function clickReview(cafe: Cafe) {
    return reviewclick.value = cafe
  }

 
  async function SaveReview() {
    loadingStore.isLoading = true;
    dialog.value = true;
    try {
      const res = await reviewService.saveReview(editReview.value);
      loadingStore.isLoading = true;
      editReview.value = res.data
      const editReviewList = ref<ReviewList>({
        review: { id: editReview.value.id },
        cafe: { id: reviewclick.value.id },
        cafesid: reviewclick.value.id
      });
      const reviewlist = await reviewService.saveReviewList(editReviewList.value)
      getReviewByUser(user.id)
      getReviewListByCafe(reviewclick.value.id!)
      // users.value = res.data;
      /*reviewlistbycafe.value = res.data
            console.log(reviewlistbycafe.value) */
      //messageStore.showError("สามารถบันทึก Review สำเร็จ");

    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Review ได้");
    }
    editReview.value.message = ""
    editReview.value.srore = 0
    loadingStore.isLoading = false;
    // editReview.value.id = -1
    // editReview.value.UsersId =user.id
  }

  function SaveClear() {
    loadingStore.isLoading = true;
  }
  return {
    reviewbyuser,
    getReviewByUser,
    reviewlistbycafe,
    getReviewListByCafe,
    dialog,
    editReview,
    SaveReview,
    SaveClear,
    reviewclick,
    clickReview
  };
});