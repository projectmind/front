import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import router from "@/router";
export const useAuthStore = defineStore("auth", () => {
  const authName = ref("");
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const getUser = () => {
    const userString = localStorage.getItem("user");
    if (!userString) return null;
    const user = JSON.parse(userString ?? "");
    return user;
  };

  const isauth = computed(() => {
    // authName is not empty
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  });
  const login = async (userName: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(userName, password);

      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      localStorage.setItem("role", JSON.stringify(res.data.user.role));//ตรงนี้
      console.log("Success");
      console.log(res.data.user.role);

      console.log(localStorage.getItem("role"))
      //router.push("/");
      router.push("/cafe"); //path
    } catch (error) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;

    // authName.value = userName;
    //  localStorage.setItem("authName", userName);
  };
  const logout = () => {
    // authName.value = "";
    loadingStore.isLoading = true;
    try {
      localStorage.removeItem("user");
      localStorage.removeItem("token");
      localStorage.removeItem("role");
      loadingStore.isLoading = true;
      router.replace("/login");
      // router.push("/login")
    } catch (error) {
      console.log(error)
    }
    // loadingStore.isLoading = true;

    loadingStore.isLoading = false;


  };
  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };
  return { login, logout, isauth, isLogin, getUser };
});
