import type Cafe from "@/types/Cafe";
import http from "./axios";
import user from "./user";
function getCafalist() {
  return http.get("/cafalist");
}

function getCafalistCategogy(id:number) {
  return http.get(`/cafalist/category/${id}`);
}

function getCafeItem(id:number,cafe:Cafe){
  return http.get(`/cafe/${id}`)
}
function getCafe(){
  return http.get("/cafe")
}

function updateCafe(id: number, cafe:Cafe){
  return http.patch(`/cafe/${id}`,cafe)
}
// function saveCafe(cafe:Cafe& { files: File[] }){
//   const formData = new FormData();
//   formData.append("cafename",cafe.cafename);
//   formData.append("description", cafe.description);
//   formData.append("location", cafe.location);
//   formData.append("file", cafe.files[0]);
//   return http.post("/cafe", formData, {
//     headers: {
//       "Content-Type": "multipart/form-data",
//     },
//   });
//   // return http.post("/cafe", cafe);
// }

function saveCafe(cafe:Cafe){
  return http.post("/cafe", cafe);
}
function deleteCafe(id: number) {
  return http.delete(`/cafe/${id}`);
}
export default {getCafalist,getCafalistCategogy,getCafeItem ,getCafe,updateCafe,saveCafe,deleteCafe};