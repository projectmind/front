import type Review from "@/types/Review";
import http from "./axios";
import type ReviewList from "@/types/ReviewList";

function getReviewByUser(id:number) {
  return http.get(`/review/user/${id}`);
}

function getReviewListByCafe(id:number) {
  return http.get(`/reviewlist/cafe/${id}`);
}
function saveReview(review: Review) {
  return http.post("/review", review);
}

function saveReviewList(reviewlist: ReviewList) {
  return http.post("/reviewlist", reviewlist);
}

export default { getReviewByUser,getReviewListByCafe ,saveReview,saveReviewList};