import type Favorite from "@/types/Favorite";
import http from "./axios";

function getFavoriteByUser(id:number) {
  return http.get(`/favorite/user/${id}`);
}
// function saveFavorite(favorite: Favorite) {
//   return http.post("/favorite", favorite);
// }

function saveFavorite(favorite: Favorite) {
  return http.post("/favorite", favorite);
}

export default { getFavoriteByUser,saveFavorite };