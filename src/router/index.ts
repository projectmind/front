import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from "@/views/LoginView.vue";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // {
    //   path: '/',
    //   name: 'home',
    //   component: HomeView
    // },
    {
      path: "/login",
      name: "login",
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/",
      name: "home",
      // components: {
      //   default: HomeView,
      //   //menu: () => import("@/components/menus/MainMenu.vue"),
      //   header: () => import("@/components/headers/MainHeader.vue"),
      // },
      /* components: {
        default: () => import("../views/cafe/CafeAllView.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      }, */
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: "/cafe",
      name: "cafe",
      components: {
        default: () => import("../views/cafe/CafeAllView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/createuser",
      name: "createuser",
      components: {
        default: () => import("../views/cafe/CreateUserView.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/cafeDrink",
      name: "cafeDrink",
      components: {
        default: () => import("../views/cafe/CafeDrink.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/cafeCheckIn",
      name: "cafeCheckIn",
      components: {
        default: () => import("../views/cafe/CafeCheckIN.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/cafeBakery",
      name: "cafeBakery",
      components: {
        default: () => import("../views/cafe/CafeBakery.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/cafeEat",
      name: "cafeEat",
      components: {
        default: () => import("../views/cafe/CafeEat.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/cafeItem",
      name: "cafecafeItem",
      components: {
        default: () => import("../views/cafe/ViewItem.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/favarite",
      name: "favarite",
      components: {
        default: () => import("../views/cafe/FavoriteView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/review",
      name: "review",
      components: {
        default: () => import("../views/cafe/ReviewView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/managecafe",
      name: "managecafe",
      components: {
        default: () => import("../views/cafe/ManageCafeView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },


  ]
});
function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
}
// router.beforeEach((to, from) => {
//   // instead of having to check every route record with
//   // to.matched.some(record => record.meta.requiresAuth)
//   if (to.meta.requiresAuth && !isLogin()) {
//     // this route requires auth, check if logged in
//     // if not, redirect to login page.
//     return {
//       path: "/login",
//       // save the location we were at to come back later
//       query: { redirect: to.fullPath },
//     };
//   }
// });

export default router
