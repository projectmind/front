export default interface User {
  id?: number;
  username?:string
  name?: string;
  surname?: string;
  password?: string;
  phone?:string
  email?:string
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
