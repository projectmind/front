export default interface Cafe {
  id?: number;
  cafename?:string
  description?: string;
  location?: string;
  srore?: number;
  image?: string;
  categoryId?: number;
}