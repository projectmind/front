import type ReviewList from "./ReviewList";
import type User from "./User";

export default interface Review {
  id?: number;
  message?: string;
  srore?: number;
  user?: User;
  reviewlist?:ReviewList
  UsersId?:number
}