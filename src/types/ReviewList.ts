import type Cafe from "./Cafe";
import type Review from "./Review";
import type User from "./User";

export default interface ReviewList {
  id?: number;  
  review?: Review;
  cafe?: Cafe;
  cafesid?:number
  
}