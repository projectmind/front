import type Cafe from "./Cafe";
import type Category from "./Category";

export default interface CafeList {
  id?: number;
  cafe:Cafe
  category:Category
  cafeId:number
  categoryId: number;
}