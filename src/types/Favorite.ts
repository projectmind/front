import type Cafe from "./Cafe";
import type User from "./User";

export default interface Favorite {
  id?: number;
  cafe?:Cafe
  user?:User
  UsersId?: number;
}